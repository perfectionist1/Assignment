<?php

function create(){
    
    if(!array_key_exists('Annex',$_SESSION)){
        $_SESSION['Annex'] = array();
    }

    if(array_key_exists('abbreviation', $_POST) && !empty($_POST['abbreviation'])){
        $_SESSION['Annex'][] = $_POST;
    }   else if(array_key_exists('elaboration', $_POST) && !empty($_POST['elaboration'])){
            $_SESSION['Annex'][] = $_POST;
        }   else{
                echo "Not defined";
            }

    header('location:index.php');
}

function get(){
    return $_SESSION['Annex'][$_GET['id']];
}

function store(){
    

if(array_key_exists('Annex',$_SESSION)){
    
    if(array_key_exists('abbreviation', $_POST) && !empty($_POST['abbreviation'])){
        $_SESSION['Annex'][$_POST['id']]['abbreviation']  = $_POST['abbreviation'] ;
         $_SESSION['Annex'][$_POST['id']]['elaboration']  = $_POST['elaboration'] ;
    }else{
        echo "Not defined";
    }
    
}
   
header('location:index.php');
}


function delete(){
    unset($_SESSION['Annex'][$_GET['id']]);
    header('location:index.php');
}

function all(){
    if(array_key_exists('Annex',$_SESSION)){
        return $_SESSION['Annex'];
    }
}

