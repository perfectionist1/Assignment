<?php
include_once("vendor/autoload.php");
include_once('lib/app.php');

//$data = get();

use Example\Crud\Profile;

$profile = new Profile();
$data = $profile->get($_GET['id']);

//debug($data);
?>

<html>
    <head>
        <title></title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
<h3> Desired Abbreviation item of Glossary. </h3>

<dl>
    <dt>Abbreviation</dt>
    <dd><?php 
        if(array_key_exists('abbreviation',$data) && !empty($data['abbreviation'])){   
            echo $data['abbreviation'];
        }else{
            echo "Not Provided";
        }
       ?></dd>
    
    <dt>Elaboration</dt>
    <dd><?php 
    if(array_key_exists('elaboration',$data) && !empty($data['elaboration'])){   
            echo $data['elaboration'];
        }else{
            echo "Not Provided";
        }
    ?></dd>
    
</dl>

<nav>
    <li><a href="index.php">List</a></li>
    <li><a href="add_new.html">Create</a></li>    
    <li><a href="edit.php?id=<?php echo $_GET['id']?>">Edit</a></li>    
</nav>

    </body>
</html>