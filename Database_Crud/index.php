<?php
include_once("vendor/autoload.php");
include_once('lib/app.php');

//use Example\Utility;

use Example\Crud\Profile;

$profile = new Profile();
$profiles = $profile->all();


//Utility::debug($profiles);
?>


<!DOCTYPE html>

<html>
    <head lang="en">
        <meta charset="UTF-8">
        <title>Glossary</title>
        <link href="css/bootstrap.min.css" rel="stylesheet">
    </head>
    
    <body>
        <p><a href="add_new.html">Click Here</a> to add a new Glossary item.</p>
            
            <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="well">
                        <h1 class="text-center">
                            Glossary
                            <a href="add_new.html" class="btn btn-primary pull-right">
                                <i class="glyphicon glyphicon-plus"></i> Add New</a>
                        </h1>
                    <table class="table table-responsive table-striped table-bordered table-hover">
                        <tr>
                            <th class="text-center" width="7%">Sl</th>
                            <th class="text-center" width="15%">Abbreviation</th>
                            <th class="text-center" width="60%">Elaboration</th>
                            <th class="text-center" width="20%"> Action </th>
                        </tr>
                        <?php
           
                            if( !empty($profiles)){
                            foreach($profiles as $key=>$value){

                        ?>
                        <tr>
                            <td><?php echo $key+1;?></td>
                            <td><?php 
                                    if(array_key_exists('abbreviation', $value) && !empty($value['abbreviation'])){
                                    echo $value['abbreviation'];
                                    }
                                ?>
                            </td>
                            <td><?php 
                                    if(array_key_exists('elaboration', $value) && !empty($value['elaboration'])){
                                    echo $value['elaboration'];
                                    }
                                ?>
                            </td>
                            
                            <td class="text-center">
                                <a href="show.php?id=<?php echo $value['id'] ;  ?>" title="View">
                                    <i class="glyphicon glyphicon-dashboard">                                                               </i>
                                </a>
                                <a href="edit.php?id=<?php echo $value['id'];  ?>" title="Edit">
                                    <i class="glyphicon glyphicon-edit"></i>
                                </a>
                                <a href="delete.php?id=<?php echo $value['id'];  ?>" title="Delete">                                    <i class="glyphicon glyphicon-trash"></i>
                                </a>
                            </td>
                        </tr>
                        <?php
                            }
                          }else{
                        ?>
                        <tr><td colspan="4">
                            No data available
                            </td>  
                        </tr>
            
                        <?php
                       }
                        ?>
                    </table>
                    <nav class="text-center">
                        <ul class="pagination">
                            <li class="disabled">
                                <a href="#" aria-label="Previous"><span aria-hidden="true">&laquo;</span>
                                </a>
                            </li>
                            <li class="active">
                                <a href="#">1 <span class="sr-only">(current)</span></a>
                            </li>
                            <li class=""><a href="#">2 <span class="sr-only">(current)</span></a></li>
                            <li class=""><a href="#">3 <span class="sr-only">(current)</span></a></li>
                            <li class=""><a href="#">4 <span class="sr-only">(current)</span></a></li>
                            <li class="disabled">
                                <a href="#" aria-label="Next"><span aria-hidden="true">&raquo;</span>
                                </a>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </body>
</html>