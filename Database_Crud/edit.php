<?php
include_once("vendor/autoload.php");
include_once('lib/app.php');

use Example\Crud\Profile;

$glossaryEdit = new Profile();

$data = $glossaryEdit-> get($_GET['id']);


?>

<!DOCTYPE html>
<html>
    <head lang="en">
        <meta charset="UTF-8">
        <title>Glossary - Add New Page</title>
        <link href="css/bootstrap.min.css" rel="stylesheet">
    </head>
    <body>
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="well">
                    <h2>Add New Glossary</h2>
                    <form action="store.php" method="post">
                       <input type="hidden" name="id" value="<?php echo $_GET['id']?>" />
                        <div class="panel panel-body">
                            <div class="row">
                                <div class="col-md-3">
                                    <label for="1">Abbreviation</label>
                                    <input id="1" class="form-control" name="abbreviation" autofocus="autofocus" placeholder="Type Short Name Here...." value="<?php if(array_key_exists('abbreviation',$data) && !empty($data['abbreviation'])){   
            echo $data['abbreviation'];
        }else{
            echo "";
        } ?>"/>
                                </div>
                                <div class="col-md-9">
                                    <label for="2">Stand for (Elaboration)</label>
                                    <input id="2" class="form-control" name="elaboration" autofocus="autofocus" placeholder="Type Elaboration-form Here...." value="<?php if(array_key_exists('elaboration',$data) && !empty($data['elaboration'])){   
            echo $data['elaboration'];
        }else{
            echo "";
        } ?>"/>
                                </div>
                                <div class="col-md-12">
                                    <label>Details</label>
                                    <textarea class="form-control" name="details" placeholder="                                         Enter details here" rows="6"></textarea>
                                </div>
                            </div><br>
                            <div class="row">
                                <div class="col-md-2">
                                 
                                    <input type="submit" name="btnSave" value="Submit" />
                                </div>
                            </div>
                        </div>
                    </form>
                    <a href="index.php">Back</a>
                </div>
            </div>
        </div>
    </body>
</html>
