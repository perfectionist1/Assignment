<?php

namespace Example;

class Utility {
    
    function debug($data){
        echo "<pre>";
        print_r($data);
        //var_dump($data);
        echo "</pre>";
    }

    static function redirect($url = 'index.php'){
       header('location:'.$url);
    }
    
}
