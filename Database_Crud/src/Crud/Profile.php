<?php

namespace Example\Crud;

use Example\Utility;

class Profile {

    public $abbreviation = '';
    public $elaboration = '';
    public $data = array();
    
    private $conn = "";

    function __construct() {
        $this->conn = new \PDO('mysql:host=localhost;dbname=quickqc', 'root', '');
        $this->conn->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
    }

    public function all() {

        try {
            $query = "SELECT * FROM glossaries";

            $_result = $this->conn->query($query);

            foreach ($_result as $row) {
                $this->data[] = $row;
            }

            return $this->data;
        } catch (PDOException $e) {
            echo 'ERROR: ' . $e->getMessage();
        }

        return $this->data;
    }

    public function create() {

        $conn = mysql_connect('localhost', 'root', '');
        mysql_select_db('quickqc');

        $query = "INSERT INTO `quickqc`.`glossaries` (`id`, `abbreviation`, `elaboration`) VALUES " . "(NULL, '".$_POST['abbreviation']."', '".$_POST['elaboration']."')";
        //mysql_query($query);
        
        if (mysql_query($query)) {
            $_SESSION['message'] = "Dear Admin, You have successfully added an Abbreviation for Glossary Page.";
            $sms = $_SESSION['message'];
            echo $sms;
             //Utility::redirect();
        } else {
            $_SESSION['message'] = "Fail to add.";
            echo mysql_error();
        }
        
       
    }

    public function get($id = null) {

        //echo $id; //input directly from user
        try {

            $data = $this->conn->query('SELECT * FROM '
                    . 'glossaries WHERE id =' . 
                    $this->conn->quote($id));

            $stmt = $this->conn->prepare('SELECT * FROM glossaries WHERE id = :id');
            $stmt->execute(array('id' => $id));
            $row = $stmt->fetch();

            return $row;



        }   catch (PDOException $e) {
                echo 'ERROR: ' . $e->getMessage();
            }
    }

    public function store() {
        $conn = mysql_connect('localhost', 'root', '');
        mysql_select_db('quickqc');
        
        $query = "UPDATE `quickqc`.`glossaries` SET `abbreviation` = '".$_POST['abbreviation']."', `elaboration` = '".$_POST['elaboration']."' WHERE `glossaries`.`id` =".$_POST['id'];
       
        if (mysql_query($query)) {
            $_SESSION['message'] = "Dear Admin, You have successfully updated the Abbreviation for Glossary Page.";
            $sms = $_SESSION['message'];
            echo $sms;
             //Utility::redirect();
        } else {
            $_SESSION['message'] = "Fail to add.";
            echo mysql_error();
        }

        //redirect();
    }

    public function delete() {
        $conn = mysql_connect('localhost', 'root', '');
        mysql_select_db('quickqc');

        $query = "DELETE FROM `quickqc`.`glossaries` WHERE `glossaries`.`id` = ".$_GET['id'];
        mysql_query($query);
        
        if (mysql_query($query)) {
            $_SESSION['message'] = "Dear Admin, You have successfully deleted an Abbreviation from Glossary Page.";
            $sms = $_SESSION['message'];
            echo $sms;
            
        }   else {
                $_SESSION['message'] = "Fail to add.";
                echo mysql_error();
            }
        
    }
    
    public function __call($fncName, $arg){
        echo "Hey man". $fncName. "is not set yet";
    }

}
