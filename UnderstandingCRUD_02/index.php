<?php

include_once('library/application.php');

    $MyEmails = $_SESSION['MyEmails'];
  
?>

<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title> <?php echo constant('PAGE_TITLE'); ?> </title>
        
    </head>
    <body>
        <section>
            <div>
                <p> <a href="create.html"> Click Here </a> to create a new email.</p>
            </div>
            <div>
                <?php 
                    if(array_key_exists('sess_message',$_SESSION) && !empty($_SESSION['sess_message'])){
                        echo $_SESSION['sess_message'] ;
                        echo "<br/>";
                    }            
                ?>
            </div>
            <table border="1">
                <tr>
                    <td> Serial</td>
                    <td> Email</td>
                    <td> Action </td>
                </tr>
                <?php 
                    $counter=1;
                    foreach($MyEmails as $email) { 
                        
                 ?>
                <tr>
                    <td> <?php echo $counter++ ?> </td>
                   <td> <?php echo $email;?> </td>
                        
                    
                    <td>    
                        <a href="#">View</a>
                        <a href="#">Edit</a>
                        <a href="#">Delete</a> 
                    </td>
                </tr>
                <?php } ?>    
                    
            </table>
            
        </section>
    </body>
</html>
