<?php

	// var_dump to output_add_rewrite_var
	// Return value of isset()
	
	$a = "test";
	$b = "anothertest";
	
	var_dump(isset($a));	//true
	echo "<br/>";
	var_dump(isset($a,$b)); //false
	
	unset ($a);
	
	var_dump(isset($a));	//false
	var_dump(isset($b));
	
	$foo = NULL;
	var_dump(isset($foo));
	
?>