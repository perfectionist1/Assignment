
<?php
    //session_start();
    
    
//    function debug($var)
//    {
//        if(is_array($var))  {
//            echo "<pre>";
//            print_r( $var );
//            echo "</pre>";
//                            }
//            else{
//            var_dump($var);
//                }
//    }
//   
//    debug($_POST); 
    
//    echo "<hr/>";
//    
//    echo print_r ($_POST['Last']);
//    echo print_r ($_POST['Middle']);
//    echo print_r ($_POST['DOB ']);
?>


<!DOCTYPE html>
<html>
    
    <head>
        <title>Form of a Death Certificate </title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="form.css" rel="stylesheet" type="text/css"/>
    </head>
    
    <body>
        <section>
			<form>
                                <h1>Appendix A-Sample US Death Certificate Form</h1>
                                    <br/>
                                <h2> Death Reporting For Vital Records </h2
                                   <br/>
                                <h3> Here is the all Detail Information (Below) you have submitted to save </h3>
                                    <br/>
                               <fieldset>
					<legend>
                                            <h3> Decedent's Name (Include AKA's if any) <h3>
					</legend>
					<ul>
                                                <li>
							<label> Last Name: </label>
                                                        <?php 
                                                            if(array_key_exists('Last',$_POST) && !empty ($_POST['Last']))
                                                                {
                                                              echo $_POST['Last'];   
                                                                }
                                                            else    {
                                                                echo " Not Provided, Please Enter.";
                                                                    }
                                                        
                                                        ?>
						</li>
                                                <br/>
						<li>
							<label> First Name:</label>
							<?php 
                                                            if(array_key_exists('First',$_POST) && !empty ($_POST['First']))
                                                                {
                                                                echo $_POST['First'];   
                                                                }
                                                            else    {
                                                                    echo " Not Provided, Please Enter.";
                                                                    }
                                                        ?>
						</li>
                                                <br/>
						<li>
							<label> Middle Name:</label>
							<?php 
                                                            if(array_key_exists('Middle',$_POST) && !empty ($_POST['Middle']))
                                                                {
                                                              echo $_POST['Middle'];   
                                                                }
                                                            else {
                                                                echo " Not Provided, Please Enter.";
                                                            }
                                                        ?>
						</li>
                                                <br/>
                                                <li>
							<label> Date Of Birth:</label>
							<?php 
                                                            if(array_key_exists('DOB',$_POST) && !empty ($_POST['DOB']))
                                                                {
                                                              echo $_POST['DOB'];   
                                                                }
                                                            else {
                                                                echo " Not Provided, Please Enter.";
                                                            }
                                                        ?>
						</li>
                                                <br/>
                                                <li>
							<label> Gender:</label>
							<?php 
                                                            if(array_key_exists('Gender',$_POST) && !empty ($_POST['Gender']))
                                                                {
                                                              echo $_POST['Gender'];   
                                                                }
                                                            else {
                                                                echo " Not Provided, Please Enter.";
                                                            }
                                                        ?>
						</li>
                                                <br/>
                                                <li>
							<label> Social Security Number:</label>
							<?php 
                                                            if(array_key_exists('SSN',$_POST) && !empty ($_POST['SSN']))
                                                                {
                                                              echo $_POST['SSN'];   
                                                                }
                                                            else {
                                                                echo " Not Provided, Please Enter.";
                                                            }
                                                        ?>
						</li>
                                                <br/>
                                                <li>
							<label> Facility Name:</label>
							<?php 
                                                            if(array_key_exists('Facility',$_POST) && !empty ($_POST['Facility']))
                                                                {
                                                              echo $_POST['Facility'];   
                                                                }
                                                            else {
                                                                echo " Not Provided, Please Enter.";
                                                            }
                                                        ?>
						</li>
					</ul>
					
				</fieldset>
                            
                                    <br/>
                                    
                                <fieldset>
					<legend>
                                            <h3> Decedent of Hispanic Origin </h3>
					</legend>
					<ul>
                                                <br/>
						<li>
							<label> Decedent of Hispanic Origin: &nbsp; </label>
                                                        <?php 
                                                            if(array_key_exists('Decedent',$_POST) && !empty ($_POST['Decedent']))
                                                                {
                                                              echo $_POST['Decedent'];   
                                                                }
                                                            else {
                                                                echo " Not Provided, Please Enter Any Option For Hispanic Origin.";
                                                                  }
                                                        ?>
						</li>
                                                <br/>
					</ul>
				</fieldset>
                            
                                <br/>
                                <fieldset>
					<legend>
						<h3> Decedent's Race </h3>
					</legend>
					<ul>
                                                <br/>
						<li>
							<label> Decedent of Hispanic Origin: &nbsp; </label>
                                                        <?php 
                                                            if(array_key_exists('Decedent_Race',$_POST) && !empty ($_POST['Decedent_Race']))
                                                                {
                                                              echo $_POST['Decedent_Race'];   
                                                                }
                                                            else {
                                                                echo " Not Provided, Please Enter Any Option For Decedent's Race.";
                                                                  }
                                                        ?>
						</li>
                                                <br/>
					</ul>
				</fieldset>
                                <br/>
                                <fieldset>
					<legend>
						<h3> Item Must be Completed by the Person Who Pronounces or Certificates Death. </h3>
					</legend>
					<ul>
                                                <br/>
						<li>
							<label> Date Pronounced Dead: &nbsp; </label>
                                                        <?php 
                                                            if(array_key_exists('DatePronounced',$_POST) && !empty ($_POST['DatePronounced']))
                                                                {
                                                              echo $_POST['DatePronounced'];   
                                                                }
                                                            else {
                                                                echo " Not Provided, Please Enter Valid Date.";
                                                                  }
                                                        ?>
						</li>
                                                <br/>
                                                <li>
							<label> License Number: &nbsp; </label>
                                                        <?php 
                                                            if(array_key_exists('LicenseNumber',$_POST) && !empty ($_POST['LicenseNumber']))
                                                                {
                                                              echo $_POST['LicenseNumber'];   
                                                                }
                                                            else {
                                                                echo " Not Provided, Please Enter License Numbe.";
                                                                  }
                                                        ?>
						</li>
                                                <br/>
                                                <li>
							<label> Actual Or Presumed Time Of Death: &nbsp; </label>
                                                        <?php 
                                                            if(array_key_exists('Actual_Or_Presumed_Time',$_POST) && !empty ($_POST['Actual_Or_Presumed_Time']))
                                                                {
                                                              echo $_POST['Actual_Or_Presumed_Time'];   
                                                                }
                                                            else {
                                                                echo " Not Provided, Please Enter Actual Or Presumed Time.";
                                                                  }
                                                        ?>
						</li>
                                                <br/>
                                                <li>
							<label> Time Pronounced Dead: &nbsp; </label>
                                                        <?php 
                                                            if(array_key_exists('TimePronounced',$_POST) && !empty ($_POST['TimePronounced']))
                                                                {
                                                              echo $_POST['TimePronounced'];   
                                                                }
                                                            else {
                                                                echo " Not Provided, Please Enter Valid Time Pronounced.";
                                                                  }
                                                        ?>
						</li>
                                                <br/>
                                                <li>
							<label> Date Signed: &nbsp; </label>
                                                        <?php 
                                                            if(array_key_exists('DateSigned',$_POST) && !empty ($_POST['DateSigned']))
                                                                {
                                                              echo $_POST['DateSigned'];   
                                                                }
                                                            else {
                                                                echo " Not Provided, Please Enter Date Signed.";
                                                                  }
                                                        ?>
						</li>
                                                <br/><li>
							<label> Was Medical Examiner Or Coroner Contacted? &nbsp; </label>
                                                        <?php 
                                                            if(array_key_exists('MadicalExaminer_or_CoronerContacted',$_POST) && !empty ($_POST['MadicalExaminer_or_CoronerContacted']))
                                                                {
                                                              echo $_POST['MadicalExaminer_or_CoronerContacted'];   
                                                                }
                                                            else {
                                                                echo " Not Provided, Please Enter About Medical Examiner/Coroner.";
                                                                  }
                                                        ?>
						</li>
                                                <br/><li>
							<label> Signature Of Person  Pronouncing Death: &nbsp; </label>
                                                        <?php 
                                                            if(array_key_exists('Signature_Of_Person',$_POST) && !empty ($_POST['Signature_Of_Person']))
                                                                {
                                                              echo $_POST['Signature_Of_Person'];   
                                                                }
                                                            else {
                                                                echo " Not Provided, Please Enter Signature Of the Person.";
                                                                  }
                                                        ?>
						</li>
                                                <br/><li>
							<label> Actual Or Presumed Date of Birth: &nbsp; </label>
                                                        <?php 
                                                            if(array_key_exists('Actual_Or_Presumed',$_POST) && !empty ($_POST['Actual_Or_Presumed']))
                                                                {
                                                              echo $_POST['Actual_Or_Presumed'];   
                                                                }
                                                            else {
                                                                echo " Not Provided, Please Enter Actual Or Presumed Date.";
                                                                  }
                                                        ?>
						</li>
                                                <br/>
					</ul>
				</fieldset>
                                <br/>
                                <fieldset>
					<legend>
						<h3> Cause Of Death (See instruction and Examples)</h3>
					</legend>
					<ul>
                                                <br/>
						<li>
							<label> a. Immediate Cause (Final desease or condition resulting in death): &nbsp; </label>
                                                        <?php 
                                                            if(array_key_exists('ImmediateCause',$_POST) && !empty ($_POST['ImmediateCause']))
                                                                {
                                                              echo $_POST['ImmediateCause'];   
                                                                }
                                                            else {
                                                                echo " Not Provided, Please Enter Valid Date Pronounced.";
                                                                  }
                                                        ?>
						</li>
                                                <br/>
                                                <li>
							<label> b. Sequentially List Conditions, (if any, leading to the cause listed on line a.) &nbsp; </label>
                                                        <?php 
                                                            if(array_key_exists('SequentiallyListConditions',$_POST) && !empty ($_POST['SequentiallyListConditions']))
                                                                {
                                                              echo $_POST['SequentiallyListConditions'];   
                                                                }
                                                            else {
                                                                echo " Not Provided, Please Enter List Conditions if any";
                                                                  }
                                                        ?>
						</li>
                                                <br/>
                                                <li>
							<label> c. Enter the Underlying Cause, (disease or injury that initiated the events resulting in death) &nbsp; </label>
                                                        <?php 
                                                            if(array_key_exists('UnderlyingCause',$_POST) && !empty ($_POST['UnderlyingCause']))
                                                                {
                                                              echo $_POST['UnderlyingCause'];   
                                                                }
                                                            else {
                                                                echo " Not Provided, Please Enter Actual Or Presumed Time.";
                                                                  }
                                                        ?>
						</li>
                                                <br/>
                                                <li>
							<label> d.Last &nbsp; </label>
                                                        <?php 
                                                            if(array_key_exists('Last',$_POST) && !empty ($_POST['Last']))
                                                                {
                                                              echo $_POST['Last'];   
                                                                }
                                                            else {
                                                                echo " Not Provided any text.";
                                                                  }
                                                        ?>
						</li>
                                                <br/>
                                                <li>
							<label> Due to (or as a consequence of): &nbsp; </label>
                                                        <?php 
                                                            if(array_key_exists('consequence1',$_POST) && !empty ($_POST['consequence1']))
                                                                {
                                                              echo $_POST['consequence1'];   
                                                                }
                                                            else {
                                                                echo " Not Provided, Please Enter any reason.";
                                                                  }
                                                        ?>
						</li>
                                                <br/><li>
							<label> Due to (or as a consequence of): &nbsp; </label>
                                                        <?php 
                                                            if(array_key_exists('consequence2',$_POST) && !empty ($_POST['consequence2']))
                                                                {
                                                              echo $_POST['consequence2'];   
                                                                }
                                                            else {
                                                                echo " Not Provided, Please Enter any reason.";
                                                                  }
                                                        ?>
						</li>
                                                <br/><li>
							<label> Due to (or as a consequence of): &nbsp; </label>
                                                        <?php 
                                                            if(array_key_exists('consequence3',$_POST) && !empty ($_POST['consequence3']))
                                                                {
                                                              echo $_POST['consequence3'];   
                                                                }
                                                            else {
                                                                echo " Not Provided, Please Enter any reason.";
                                                                  }
                                                        ?>
						</li>
                                                <br/><li>
							<label> Onset of death: &nbsp; </label>
                                                        <?php 
                                                            if(array_key_exists('Onset_of_death1',$_POST) && !empty ($_POST['Onset_of_death1']))
                                                                {
                                                              echo $_POST['Onset_of_death1'];   
                                                                }
                                                            else {
                                                                echo " Not Provided Onset of Death.";
                                                                  }
                                                        ?>
						</li>
                                                <br/>
                                                <li>
							<label> Onset of death: &nbsp; </label>
                                                        <?php 
                                                            if(array_key_exists('Onset_of_death2',$_POST) && !empty ($_POST['Onset_of_death2']))
                                                                {
                                                              echo $_POST['Onset_of_death2'];   
                                                                }
                                                            else {
                                                                echo " Not Provided Onset of Death.";
                                                                  }
                                                        ?>
						</li>
                                                <br/>
                                                <li>
							<label> Onset of death: &nbsp; </label>
                                                        <?php 
                                                            if(array_key_exists('Onset_of_death3',$_POST) && !empty ($_POST['Onset_of_death3']))
                                                                {
                                                              echo $_POST['Onset_of_death3'];   
                                                                }
                                                            else {
                                                                echo " Not Provided Onset of Death.";
                                                                  }
                                                        ?>
						</li>
                                                <br/>
                                                <li>
							<label> Onset of death: &nbsp; </label>
                                                        <?php 
                                                            if(array_key_exists('Onset_of_death4',$_POST) && !empty ($_POST['Onset_of_death4']))
                                                                {
                                                              echo $_POST['Onset_of_death4'];   
                                                                }
                                                            else {
                                                                echo " Not Provided Onset of Death.";
                                                                  }
                                                        ?>
						</li>
                                                <br/>
                                                <li>
							<label> <b> PART II. </b> Enter other significant conditions:  &nbsp; </label>
                                                        <?php 
                                                            if(array_key_exists('PART_II_Text',$_POST) && !empty ($_POST['PART_II_Text']))
                                                                {
                                                              echo $_POST['PART_II_Text'];   
                                                                }
                                                           else {
                                                                echo " Not Provided Onset of Death.";
                                                                  }
                                                        ?>
						</li>
                                                <br/>
                                                <li>
							<label> Was an Autospy Performed?  &nbsp; </label>
                                                        <?php 
                                                            if(array_key_exists('AutospyPerformed',$_POST) && !empty ($_POST['AutospyPerformed']))
                                                                {
                                                              echo $_POST['AutospyPerformed'];   
                                                                }
                                                           else {
                                                                echo " Not Provided info about Autospy.";
                                                                  }
                                                        ?>
						</li>
                                                <br/>
                                                <li>
							<label> Were Autospy Findings Available To Complete The Cause of Death?  &nbsp; </label>
                                                        <?php 
                                                            if(array_key_exists('AutospyFindings',$_POST) && !empty ($_POST['AutospyFindings']))
                                                                {
                                                              echo $_POST['AutospyFindings'];   
                                                                }
                                                           else {
                                                                echo " Not Provided info about Autospy Findings.";
                                                                  }
                                                        ?>
						</li>
                                                <br/>
                                                <li>
							<label> Did Tobacco Use Contribute To Death?  &nbsp; </label>
                                                        <?php 
                                                            if(array_key_exists('TobaccoUsed',$_POST) && !empty ($_POST['TobaccoUsed']))
                                                                {
                                                              echo $_POST['TobaccoUsed'];   
                                                                }
                                                           else {
                                                                echo " Not Provided info about Tobacco";
                                                                  }
                                                        ?>
						</li>
                                                <br/>
					</ul>
				</fieldset>
                                <fieldset> 
                                    <ligend>          <h3> If Female: </h3> </ligend>
                                        <ul>
                                             <li>
							<label> For Female  &nbsp; </label>
                                                        <?php 
                                                            if(array_key_exists('femalePart',$_POST) && !empty ($_POST['femalePart']))
                                                                {
                                                              echo $_POST['femalePart'];   
                                                                }
                                                           else {
                                                                echo " Not Provided info about when the person is female";
                                                                  }
                                                        ?>
						</li>
                                                <br/>
					</ul>
				</fieldset>
                                <fieldset> 
                                    <ligend>     <h3> Manner Of Death </h3> </ligend>
                                        <ul>
                                             <li>
							<label> For Female  &nbsp; </label>
                                                        <?php 
                                                            if(array_key_exists('MannerOfDeath',$_POST) && !empty ($_POST['MannerOfDeath']))
                                                                {
                                                              echo $_POST['MannerOfDeath'];   
                                                                }
                                                           else {
                                                                echo " Not Provided info about the manner of death.";
                                                                  }
                                                        ?>
						</li>
                                                <br/>
					</ul>
				</fieldset>
                                 <fieldset> 
                                    <ligend>     <h3> Injury </h3> </ligend>
                                        <ul>
                                             <li>
							<label> Date Of Injury &nbsp; </label>
                                                        <?php 
                                                            if(array_key_exists('Date_Of_Injury',$_POST) && !empty ($_POST['Date_Of_Injury']))
                                                                {
                                                              echo $_POST['Date_Of_Injury'];   
                                                                }
                                                           else {
                                                                echo " Not Provided info about date of injury.";
                                                                  }
                                                        ?>
						</li>
                                                <br/>
                                                <li>
							<label> Time Of Injury &nbsp; </label>
                                                        <?php 
                                                            if(array_key_exists('Time_Of_Injury',$_POST) && !empty ($_POST['Time_Of_Injury']))
                                                                {
                                                              echo $_POST['Time_Of_Injury'];   
                                                                }
                                                           else {
                                                                echo " Not Provided info about Time Of Injury.";
                                                                  }
                                                        ?>
						</li>
                                                <br/>
                                                <li>
							<label> Place Of Injury: &nbsp; </label>
                                                        <?php 
                                                            if(array_key_exists('Place_Of_Injury',$_POST) && !empty ($_POST['Place_Of_Injury']))
                                                                {
                                                              echo $_POST['Place_Of_Injury'];   
                                                                }
                                                           else {
                                                                echo " Not Provided info about place Of Injury.";
                                                                  }
                                                        ?>
						</li>
                                                <br/>
                                                <li>
							<label> Injury At Works? &nbsp; </label>
                                                        <?php 
                                                            if(array_key_exists('Injury_At_Works',$_POST) && !empty ($_POST['Injury_At_Works']))
                                                                {
                                                              echo $_POST['Injury_At_Works'];   
                                                                }
                                                           else {
                                                                echo " Not Provided info about whether the person is injured as works or not?.";
                                                                  }
                                                        ?>
						</li>
                                                <br/>
                                                
					</ul>
				</fieldset>
                                <fieldset> 
                                    <ligend>     <h3> Location Of Injury </h3> </ligend>
                                        <ul>
                                             <li>
							<label> State: &nbsp; </label>
                                                        <?php 
                                                            if(array_key_exists('State',$_POST) && !empty ($_POST['State']))
                                                                {
                                                              echo $_POST['State'];   
                                                                }
                                                           else {
                                                                echo " Not Provided info about State.";
                                                                  }
                                                        ?>
						</li>
                                                <br/>
                                                <li>
							<label> City Or Town: &nbsp; </label>
                                                        <?php 
                                                            if(array_key_exists('City_Or_Town',$_POST) && !empty ($_POST['City_Or_Town']))
                                                                {
                                                              echo $_POST['City_Or_Town'];   
                                                                }
                                                           else {
                                                                echo " Not Provided info about City/Town.";
                                                                  }
                                                        ?>
						</li>
                                                <br/>
                                                <li>
							<label> Street and Number: &nbsp; </label>
                                                        <?php 
                                                            if(array_key_exists('Street_and_Number',$_POST) && !empty ($_POST['Street_and_Number']))
                                                                {
                                                              echo $_POST['Street_and_Number'];   
                                                                }
                                                           else {
                                                                echo " Not Provided info about Street and Number.";
                                                                  }
                                                        ?>
						</li>
                                                <br/>
                                                <li>
							<label> Apartment No: &nbsp; </label>
                                                        <?php 
                                                            if(array_key_exists('Apartment_No',$_POST) && !empty ($_POST['Apartment_No']))
                                                                {
                                                              echo $_POST['Apartment_No'];   
                                                                }
                                                           else {
                                                                echo " Not Provided info about whether the person is injured as works or not?.";
                                                                  }
                                                        ?>
						</li>
                                                <br/>
                                                <li>
							<label> Zip Code: &nbsp; </label>
                                                        <?php 
                                                            if(array_key_exists('Zip_Code',$_POST) && !empty ($_POST['Zip_Code']))
                                                                {
                                                              echo $_POST['Zip_Code'];   
                                                                }
                                                           else {
                                                                echo " Not Provided info about zip code.";
                                                                  }
                                                        ?>
						</li>
                                                <br/>
                                        </ul>
				</fieldset>
                                <fieldset>
                                    <ligend> <h3> Describe How Injury Occurred </h3> </ligend> <br/>
                                    
                                                        <?php 
                                                            if(array_key_exists('How_Injury_Occurred',$_POST) && !empty ($_POST['How_Injury_Occurred']))
                                                                {
                                                              echo $_POST['How_Injury_Occurred'];   
                                                                }
                                                           else {
                                                                echo " Not Provided info about the procedure of injury is been occurred.";
                                                                  }
                                                        ?>
                                </fieldset>
                                <fieldset> 
                                    <ligend>     <h3> If Transportation Injury </h3> </ligend>
                                        <ul>
                                             <li>
							<label> For Female  &nbsp; </label>
                                                        <?php 
                                                            if(array_key_exists('TransportationInjury',$_POST) && !empty ($_POST['TransportationInjury']))
                                                                {
                                                              echo $_POST['TransportationInjury'];   
                                                                }
                                                           else {
                                                                echo " Not Provided info about the transportaion of injury.";
                                                                  }
                                                        ?>
						</li>
                                                <br/>
					</ul>
				</fieldset>
                                 <fieldset> 
                                    <ligend>     <h3> Certifier </h3> </ligend>
                                        <ul>
                                             <li>
							<label> Certifier &nbsp; </label>
                                                        <?php 
                                                            if(array_key_exists('Certifying',$_POST) && !empty ($_POST['Certifying']))
                                                                {
                                                              echo $_POST['Certifying'];   
                                                                }
                                                           else {
                                                                echo " Not Provided info about certifier.";
                                                                  }
                                                        ?>
						</li>
                                                <br/>
					</ul>
				</fieldset>
                                <fieldset> 
                                    <ligend>     <h3> Person Completing Cause of Death </h3> </ligend>
                                        <ul>
                                             <li>
							<label> Name: &nbsp; </label>
                                                        <?php 
                                                            if(array_key_exists('name',$_POST) && !empty ($_POST['name']))
                                                                {
                                                              echo $_POST['name'];   
                                                                }
                                                           else {
                                                                echo " Not Provided info about name.";
                                                                  }
                                                        ?>
						</li>
                                                <br/>
                                                <li>
							<label> Address: &nbsp; </label>
                                                        <?php 
                                                            if(array_key_exists('Address',$_POST) && !empty ($_POST['Address']))
                                                                {
                                                              echo $_POST['Address'];   
                                                                }
                                                           else {
                                                                echo " Not Provided info about Address.";
                                                                  }
                                                        ?>
						</li>
                                                <br/>
                                                <li>
							<label> Zip Code: &nbsp; </label>
                                                        <?php 
                                                            if(array_key_exists('ZipCode',$_POST) && !empty ($_POST['ZipCode']))
                                                                {
                                                              echo $_POST['ZipCode'];   
                                                                }
                                                           else {
                                                                echo " Not Provided info about zip code.";
                                                                  }
                                                        ?>
						</li>
                                                <br/>
                                                <li>
							<label> Name: &nbsp; </label>
                                                        <?php 
                                                            if(array_key_exists('name2',$_POST) && !empty ($_POST['name2']))
                                                                {
                                                              echo $_POST['name2'];   
                                                                }
                                                           else {
                                                                echo " Not Provided info about Name.";
                                                                  }
                                                        ?>
						</li>
                                                <br/>
                                                <li>
							<label> Address: &nbsp; </label>
                                                        <?php 
                                                            if(array_key_exists('Address2',$_POST) && !empty ($_POST['Address2']))
                                                                {
                                                              echo $_POST['Address2'];   
                                                                }
                                                           else {
                                                                echo " Not Provided info about Address.";
                                                                  }
                                                        ?>
						</li>
                                                <br/>
                                                <li>
							<label> Zip Code: &nbsp; </label>
                                                        <?php 
                                                            if(array_key_exists('ZipCode2',$_POST) && !empty ($_POST['ZipCode2']))
                                                                {
                                                              echo $_POST['ZipCode2'];   
                                                                }
                                                           else {
                                                                echo " Not Provided info about Zip Code.";
                                                                  }
                                                        ?>
						</li>
                                                <br/>
                                        </ul>
				</fieldset>
                                
			</form>
        </section>
    </body>
    
</html>