-- phpMyAdmin SQL Dump
-- version 4.4.3
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: May 17, 2015 at 08:37 অপরাহ্ণ
-- Server version: 5.6.24
-- PHP Version: 5.6.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `death_certificate`
--

-- --------------------------------------------------------

--
-- Table structure for table `decedent_name`
--

CREATE TABLE IF NOT EXISTS `decedent_name` (
  `SL` int(11) NOT NULL,
  `Last_Name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `First_Name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `Middle_Name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `Date_Of_Birth` int(11) NOT NULL,
  `Gender` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `Social_Security_Number` int(11) NOT NULL,
  `Facility_Name` varchar(64) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `decedent_name`
--

INSERT INTO `decedent_name` (`SL`, `Last_Name`, `First_Name`, `Middle_Name`, `Date_Of_Birth`, `Gender`, `Social_Security_Number`, `Facility_Name`) VALUES
(1, 'Mian', 'Zadid', 'Rusdid', 21, 'Male', 123456789, 'LiveOutSource');

-- --------------------------------------------------------

--
-- Table structure for table `decedent_of_hispanic_organic`
--

CREATE TABLE IF NOT EXISTS `decedent_of_hispanic_organic` (
  `SL` int(11) NOT NULL,
  `Spanish / Hispanic / Latino` varchar(256) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `decedent_of_hispanic_organic`
--

INSERT INTO `decedent_of_hispanic_organic` (`SL`, `Spanish / Hispanic / Latino`) VALUES
(1, 'No, not Spanish/Hispanic/Latino');

-- --------------------------------------------------------

--
-- Table structure for table `decedent_race`
--

CREATE TABLE IF NOT EXISTS `decedent_race` (
  `SL` int(11) NOT NULL,
  `Race_Type` varchar(100) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `decedent_race`
--

INSERT INTO `decedent_race` (`SL`, `Race_Type`) VALUES
(1, 'White'),
(2, 'Black or African American');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `decedent_name`
--
ALTER TABLE `decedent_name`
  ADD PRIMARY KEY (`SL`);

--
-- Indexes for table `decedent_of_hispanic_organic`
--
ALTER TABLE `decedent_of_hispanic_organic`
  ADD PRIMARY KEY (`SL`);

--
-- Indexes for table `decedent_race`
--
ALTER TABLE `decedent_race`
  ADD PRIMARY KEY (`SL`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `decedent_name`
--
ALTER TABLE `decedent_name`
  MODIFY `SL` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `decedent_of_hispanic_organic`
--
ALTER TABLE `decedent_of_hispanic_organic`
  MODIFY `SL` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `decedent_race`
--
ALTER TABLE `decedent_race`
  MODIFY `SL` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
